// [Section] Javascript Synchronous vs Asynchronous
// Javascript is by default is synchronous meaning that only one statement is executed at a time

// This can be proven when a statement has an error, javascript will not proceed with the next statement
console.log("Hello World");
//conosle.log("Hello Again");
console.log("Goodbye");

// When certain statements take a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// When an action will take some time to process, this results in code  "blocking"
console.log("Hello World");
// We might not notice it due to the fast processing power of our devices
// This is the reason why some websites don't instantly load and we only see a white screen at times while the application is still waiting for all the code to be executed
// for(let i = 0; i <= 1500; i++){
// 	console.log(i);
// }
// Only after the loop ends will this statement execute
console.log("Hello Again");

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// [Section] Getting all posts

// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
// Syntax
	// fetch('URL')
console.log(fetch('https://jsonplaceholder.typicode.com/posts')
);

// Syntax
	// fetch('URL')
	// .then((response)=>{})

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
// The "fetch" method will return a "promise" that resolves to a "Response" object
// The "then" method captures the "Reponse" object and returns another "promise" which will eventually be "resolved" or "rejected"
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json())
// Print the converted JSON value from the "fetch" request
// Using multiple "then" methods creates a "promise chain"
.then((json) => console.log(json));

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for
// Creates an asynchronous function
async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	// Result returned by fetch is a returns a promise
	console.log(result);
	// The returned "Response" is an object
	console.log(typeof result);
	// We cannot access the content of the "Response" by directly accessing it's body property
	console.log(result.body);

	// Converts the data from the "Response" object as JSON
	let json = await result.json();
	// Print out the content of the "Response" object
	console.log(json);

};

fetchData();

// [Section] Getting a specific post

// Retrieves a specific post following the Rest API (retrieve, /posts/:id, GET)
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Creating a post

// Syntax
	// fetch('URL', options)
	// .then((response)=>{})
	// .then((response)=>{})

// Creates a new post following the Rest API (create, /posts/:id, POST)
fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
	method: 'POST',
	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-type': 'application/json',
	},
	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello world!',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a post using PUT method

// Updates a specific post following the Rest API (update, /posts/:id, PUT)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	id: 1,
	  	title: 'Updated post',
	  	body: 'Hello again!',
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a post using PATCH method

// Updates a specific post following the Rest API (update, /posts/:id, Patch)
// The difference between PUT and PATCH is the number of properties being changed
// PATCH is used to update a single/several properties
// PUT is used to update the whole object
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Corrected post',
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Deleting a post

// Deleting a specific post following the Rest API (delete, /posts/:id, DELETE)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE'
});


